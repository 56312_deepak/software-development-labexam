const express=require("express")
const router=express.Router()
const db =require("../db")
const utils=require("../utils")

router.post("/addMovie",(req,res)=>{
    // Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
    const {title,releaseDate,releaseTime,directorName} =req.body
    const connection=db.fetchConnection()
    const sql=`insert into movie values(0,'${title}','${releaseDate}','${releaseTime}','${directorName}')`
    connection.query(sql,(error,data)=>{
        connection.end()
        if(error)
        res.send(utils.createResult(error))
        else
        res.send(utils.createResult(null,data))

    })
})
router.get("/getAllMovies",(req,res)=>{
    // Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
    const connection=db.fetchConnection()//to get the connection
    const sql=`select * from movie`
    connection.query(sql,(error,data)=>{
        connection.end()
        if(error)
        res.send(utils.createResult(error))
        else
        res.send(utils.createResult(null,data))

    })
})
router.put("/updateMovie",(req,res)=>{
   
    // Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
    const connection=db.fetchConnection()//to get the connection
    const {releaseDate,releaseTime,id} =req.body

    const sql=`update movie set movie_release_date ='${releaseDate}' , movie_release_time = '${releaseTime}' where movie_id =${id} `
    connection.query(sql,(error,data)=>{
        connection.end()
        if(error)
        res.send(utils.createResult(error))
        else
        res.send(utils.createResult(null,data))

    })
})
router.delete("/deleteMovie/:id",(req,res)=>{
   
    // Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
    const connection=db.fetchConnection()//to get the connection
    const {id} =req.params

    const sql=`delete from movie  where movie_id =${id} `
    connection.query(sql,(error,data)=>{
        connection.end()
        if(error)
        res.send(utils.createResult(error))
        else
        res.send(utils.createResult(null,data))

    })
})

module.exports=router