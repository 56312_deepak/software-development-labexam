const express=require("express")
const movieRouter=require("./routes/movie")
const cors=require("cors")
const app=express()
app.use(express.json())
app.use(cors("*"))
app.use("/movie",movieRouter)

app.listen(4001,()=>{
    console.log("server started on port 4001")
})