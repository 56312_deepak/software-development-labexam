create table movie(
    movie_id int primary key auto_increment,
    movie_title varchar(30),
    movie_release_date varchar(30),
    movie_release_time varchar(40),
    director_name varchar(20)
    
);

-- Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)